const products = [
  {
    shampoo: {
      price: "$50",
      quantity: 4,
    },
    "Hair-oil": {
      price: "$40",
      quantity: 2,
      sealed: true,
    },
    comb: {
      price: "$12",
      quantity: 1,
    },
    utensils: [
      {
        spoons: { quantity: 2, price: "$8" },
      },
      {
        glasses: { quantity: 1, price: "$70", type: "fragile" },
      },
      {
        cooker: { quantity: 4, price: "$900" },
      },
    ],
    watch: {
      price: "$800",
      quantity: 1,
      type: "fragile",
    },
  },
];

// convert the product into newProduct of Object
const utensilsData = products[0].utensils.map((element) => {
  return Object.entries(element).flat();
});

const utensilsConvert = utensilsData.reduce((acc, current) => {
  acc[current[0]] = current[1];
  return acc;
}, []);

const utensilsDelete = products.map(({ utensils, ...restData }) => {
  return restData;
});

const newProducts = Object.assign(utensilsDelete[0], utensilsConvert);

// Q1. Find all the items with price more than $65.

const priceItemGreater65 = Object.entries(newProducts).filter((value) => {
  return Number(value[1].price.substr(1)) > 65;
});

//console.log("Product price more than $65: "+JSON.stringify(priceItemGreater65))

// Q2. Find all the items where quantity ordered is more than 1.

const quantityGreaterThan1 = Object.entries(newProducts).filter((value) => {
  return value[1].quantity > 1;
});

//console.log("Product quantity more than 1: "+JSON.stringify(quantityGreaterThan1));

// Q.3 Get all items which are mentioned as fragile.

const fragileItem = Object.entries(newProducts).filter((value) => {
  return value[1].type === "fragile";
});

//console.log("Product type is fragile: "+JSON.stringify(fragileItem));

// Q.4 Find the least and the most expensive item for a single quantity.

const quantityEqualT01 = Object.entries(newProducts).filter((value) => {
  return value[1].quantity === 1;
});

const sortingQuantity = quantityEqualT01.sort((valueA, valueB) => {
  let first = Number(valueA[1].price.substr(1));
  let second = Number(valueB[1].price.substr(1));
  return first - second;
});

 //console.log("Least expensive item for single quantity: "+JSON.stringify(sortingQuantity[0]));
 //console.log("Most expensive item for single quantity: "+JSON.stringify(sortingQuantity[sortingQuantity.length-1]));

// Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

